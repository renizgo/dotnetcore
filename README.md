# Aplicação Dotnet Core

## Requisitos

* Instalação do comando dotnet 
```
$ wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb
$ sudo dpkg -i packages-microsoft-prod.deb
$ sudo add-apt-repository universe
$ sudo apt-get install apt-transport-https
$ sudo apt-get update
$ sudo apt-get install dotnet-sdk-2.2
```

## Criar uma app de Dotnet

Verifique a versão do Dotnet
```
$ dotnet --version
```

## Crie uma aplicação básica
```
$ cd Documentos/
$ mkdir dotnet
$ cd dotnet/
$ dotnet new webapp -n hello
$ ls
$ cd hello/
```

Rode a app com o 
```
$ dotnet run
```
